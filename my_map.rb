#!/home/hz/.rbenv/shims/ruby

module Enumerable
  def my_map
    retr = []
    self.each do |x|
      retr.push(yield(x))
    end
    return retr
  end
end

arr = [3,4,5,6,7]
puts "First round--"
puts arr.my_map { |x| x * 2 }
puts "Second round--"
puts arr.my_map { |x| x * 3 }
